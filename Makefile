# -*- mode: makefile-gmake -*-

.PHONY: clean
clean:
	rm -f anndb.csv.gz report_anndb.jsonl report_postgres_kaggle.txt report_postgres_simhash.txt report_milvus_kaggle.txt

anndb.csv.gz: sample.csv.gz	## Load data into the AnnDB database.
	cat $< | \
	zcat | \
	python -c "$$ANNDB_LOAD" | \
	gzip > $@
define ANNDB_LOAD
import anndb_api
import csv
import sys
from itertools import islice
def batch(iterable, n=10):
    i = iter(iterable)
    piece = list(islice(i, n))
    while piece:
        yield piece
        piece = list(islice(i, n))
client = anndb_api.Client('$(APIKEY)')
dataset = client.vector('$(DATASET)')
records = csv.reader(sys.stdin)
pairs = ([list(eval(r[1])), {'title': r[0]}] for r in records)
items = (anndb_api.VectorItem(None, p[0], p[1]) for p in pairs)
batches = batch(items, 100)
results = (dataset.insert_batch(b) for b in batches)
urns = (list(map(lambda x: x.id.urn, r)) for r in results)
writer = csv.writer(sys.stdout)
writer.writerows(urns)
endef
export ANNDB_LOAD

report_anndb.jsonl: sample.csv.gz ## Generate a load test report for the AnnDB database using the Kaggle dataset.
	cat $< | \
	zcat | \
	shuf -n100 | \
	python -c "$$ANNDB_TEST" > $@
define ANNDB_TEST
import anndb_api
import csv
import jsonlines
import sys
client = anndb_api.Client('$(APIKEY)')
dataset = client.vector('$(DATASET)')
records = csv.reader(sys.stdin)
pairs = ([list(eval(r[1])), {'title': r[0]}] for r in records)
results = ([p[1], list(map(lambda i: [i.id.urn, i.metadata['title']], dataset.search(p[0], 10)))] for p in pairs)
writer = jsonlines.Writer(sys.stdout)
writer.write_all(results)
endef
export ANNDB_TEST

samples.csv.gz: embeddings_kaggle.csv.gz
	cat $< | \
	zcat | \
	shuf -n $(SAMPLES) | \
	python -c "$$SAMPLE_KAGGLE" | \
	gzip > samples.csv.gz

report_postgres_kaggle.txt: embeddings_kaggle.csv.gz ## Generate a load test report for Postgres using the Kaggle dataset.
	psql -c "drop table if exists sample cascade"
	psql -c "drop extension if exists cube cascade"
	psql -c "drop type if exists language_tag cascade"
	psql -c "drop type if exists subject_tag cascade"
	psql -c "create extension if not exists cube"
	psql -c "create type language_tag as enum ('en', 'fr', 'es', 'it', 'zh', 'hi')"
	psql -c "create type subject_tag as enum ('bio', 'math', 'acct', 'phys', 'eng')"
	psql -c "create table if not exists sample (id serial primary key, q_id int, language language_tag, subject subject_tag, embedding cube)"
	psql -c "\copy sample (q_id, language, subject, embedding) from program 'cat samples.csv.gz | zcat' with (format csv, header true)"
	psql -c "create index on sample (language, subject)"
	for i in {1..$(TRANSACTIONS)} ; do pgbench -f kaggle.sql -n -t 1 -r -P 5 -DEMBEDDING="$$(psql -c 'select embedding from sample order by random() limit 1' -At)" >> $@ ; done
define SAMPLE_KAGGLE
import csv
import random
import sys
language_tag = ['en', 'fr', 'es', 'it', 'zh', 'hi']
subject_tag = ['bio', 'math', 'acct', 'phys', 'eng']
records = csv.reader(sys.stdin)
pairs = ([p[0], language_tag[random.randint(0, len(language_tag)-1)], subject_tag[random.randint(0, len(subject_tag)-1)], p[1][1]] for p in enumerate(records))
writer = csv.writer(sys.stdout)
writer.writerows(pairs)
endef
export SAMPLE_KAGGLE

report_postgres_simhash.txt: embeddings_simhash.csv.gz ## Generate a load test report for Postgres using the simhash dataset.
	cat $< | \
	zcat | \
	shuf -n $(SAMPLES) | \
	python -c "$$SAMPLE_SIMHASH" | \
	gzip > samples.csv.gz
	psql -c "drop table if exists sample cascade"
	psql -c "drop extension if exists cube cascade"
	psql -c "drop type if exists language_tag cascade"
	psql -c "drop type if exists subject_tag cascade"
	psql -c "create extension if not exists cube"
	psql -c "create type language_tag as enum ('en', 'fr', 'es', 'it', 'zh', 'hi')"
	psql -c "create type subject_tag as enum ('bio', 'math', 'acct', 'phys', 'eng')"
	psql -c "create table if not exists sample (id serial primary key, q_id int, language language_tag, subject subject_tag, embedding cube)"
	psql -c "\copy sample (q_id, language, subject, embedding) from program 'cat samples.csv.gz | zcat' with (format csv, header true)"
	psql -c "create index on sample (language, subject)"
	for i in {1..$(TRANSACTIONS)} ; do pgbench -f simhash.sql -n -t 1 -r -P 5 -DEMBEDDING="$$(psql -c 'select embedding from sample order by random() limit 1' -At)" >> $@ ; done
define SAMPLE_SIMHASH
import csv
import random
import sys
language_tag = ['en', 'fr', 'es', 'it', 'zh', 'hi']
subject_tag = ['bio', 'math', 'acct', 'phys', 'eng']
records = csv.reader(sys.stdin)
pairs = ([r[0], language_tag[random.randint(0, len(language_tag)-1)], subject_tag[random.randint(0, len(subject_tag)-1)], "({0})".format(",".join(list(r[1])))] for r in records)
writer = csv.writer(sys.stdout)
writer.writerows(pairs)
endef
export SAMPLE_SIMHASH

report_milvus_kaggle.txt: embeddings_kaggle.csv.gz ## Generate a load test report for the Milvus database using the Kaggle dataset.
	cat $< | \
	zcat | \
	shuf -n $(SAMPLES) | \
	python -c "$$TEARDOWN_KAGGLE"
	python -c "$$SETUP_KAGGLE"
	time python -c "$$TEST_KAGGLE" > $@
define SETUP_KAGGLE
import csv
import sys
from milvus import Milvus, IndexType, MetricType, Status
from itertools import islice
def batch(iterable, n=10):
    i = iter(iterable)
    piece = list(islice(i, n))
    while piece:
        yield piece
        piece = list(islice(i, n))
client = Milvus(host='localhost', port='19530')
client.create_collection({'collection_name':'kaggle', 'dimension':50, 'index_file_size':1024, 'metric_type':MetricType.L2})
records = csv.reader(sys.stdin)
pairs = ([p[0], list(eval(p[1][1]))] for p in enumerate(records))
batches = (list(zip(*b)) for b in batch(pairs, 100))
list(map(lambda x: client.insert(collection_name='kaggle', ids=list(x[0]), records=list(x[1])), batches))
endef
export SETUP_KAGGLE
define TEST_KAGGLE
from milvus import Milvus, IndexType, MetricType, Status
from random import randint
client = Milvus(host='localhost', port='19530')
targets = [client.get_entity_by_id(ids=[randint(0, $(SAMPLES))], collection_name='kaggle')[1][0] for i in range($(TRANSACTIONS))]
client.search(collection_name='kaggle', query_records=targets, top_k=10, params={'nprobe': 16})
endef
export TEST_KAGGLE
define TEARDOWN_KAGGLE
from milvus import Milvus, IndexType, MetricType, Status
client = Milvus(host='localhost', port='19530')
client.drop_collection(collection_name='kaggle')
endef
export TEARDOWN_KAGGLE
