# Databricks notebook source
# MAGIC %md
# MAGIC # Work In Progess

# COMMAND ----------

# MAGIC %md 
# MAGIC # Project Lego
# MAGIC 
# MAGIC Project Lego is the overarching project for development of our content pipeline.  
# MAGIC For more information about the project, see https://confluence.cheggnet.com/display/~kyle@chegg.com/Project+Lego+aka+Content+Pipelines
# MAGIC 
# MAGIC For information on how to access the data, see:  
# MAGIC https://confluence.cheggnet.com/display/~aphilipps@chegg.com/Project+Lego+Access  
# MAGIC https://cheggprod.cloud.databricks.com/#notebook/1014531

# COMMAND ----------

import sys

# COMMAND ----------

def getProjectLegoHelp():
  how_to = \
'''
The Datasets available in Project Lego are the following.

1. QNA Questions - getProjectLegoQNA(("questions" | "answers"))
2. TBS Problems - getProjectLegoTBS(("problems" | "solutions"), ("clean" | "raw"))
3. QNA Simhash - getProjectLegoQNASimhash() -- Deprecated
4. QNA Neighbors - getProjectLegoQNANeighbors() -- Deprecated
5. Prep - getProjectLegoPrep(("decks" | "cards"))
6. QNA Fingerprint - getProjectLegoQNAFingerprint(("qna-simhash" | "qna-neighbors" | "qna-recs" | "qna-model"))

Returns: The desired path using the Databricks mount point.
Ex. '/mnt/chegg-databricks-datascience/chegg-aws-fender-prod/chegg-databricks-datascience/teams/lsds/project-lego/qna-catalog/questions/cal_date=2020-01-07/

For more information on the datasets, please see https://confluence.cheggnet.com/display/~kyle@chegg.com/Project+Lego+aka+Content+Pipelines.
'''
  
  print(how_to)
  return

# COMMAND ----------

def getProjectLegoQNA(content_type):
  ret_content = 'qna-catalog'

  if content_type == 'questions':
    ret_content_type = 'questions'

    # Clean and Raw questions are stored in the same file
    ret_clean = ''
#   elif content_type == 'deleted':
#     ret_content_type = 'deleted'

#     # Clean and Raw questions are stored in the same file
#     ret_clean = ''
    
  elif content_type == 'answers':
    ret_content_type = 'answers'
    # Clean and Raw answers are stored in the same file
    ret_clean = ''
#     print("QNA answers do not yet exist for Project Lego. \nUse getProjectLegoHelp() to see available options.")
#     sys.exit()
#     return -1
  else:
    print("Please specify 'questions' or 'answers'. \nUse getProjectLegoHelp() to see available options.")
    sys.exit()
    return -1
  
  databricks_path = generatePaths(ret_content, ret_content_type, ret_clean)
  
  return databricks_path

# getProjectLegoQNA('answers')

# COMMAND ----------

def getProjectLegoTBS(content_type, clean):
  ret_content = 'tbs-catalog'
    
  if content_type == 'problems':
    ret_content_type = 'problems'

    ret_clean = getCleanOrRaw(clean)

  elif content_type == 'solutions':
    ret_content_type = 'solutions'

    ret_clean = getCleanOrRaw(clean)

  else:
    print("Please specify 'problems' or 'solutions'. \nUse getProjectLegoHelp() to see available options.")
    sys.exit()
    return -1
  
  databricks_path = generatePaths(ret_content, ret_content_type, ret_clean)
  
  return databricks_path

# getProjectLegoTBS('problems', 'clean')

# COMMAND ----------

def getProjectLegoQNASimhash():
  ret_content = 'qna-simhash'
  
  ret_content_type, ret_clean = '', ''
  
  databricks_path = generatePaths(ret_content, ret_content_type, ret_clean)
  
  return databricks_path

# getProjectLegoQNASimhash()

# COMMAND ----------

def getProjectLegoQNANeighbors():

  ret_content = 'qna-neighbors'
  
  ret_content_type, ret_clean = '', ''
  
  databricks_path = generatePaths(ret_content, ret_content_type, ret_clean)
  
  return databricks_path

# getProjectLegoQNANeighbors()

# COMMAND ----------

def getProjectLegoPrep(content_type):
  ret_content = 'prep'
    
  if content_type == 'cards':
    ret_content_type = 'prep-cards'

    ret_clean = '/clean'

  elif content_type == 'decks':
    ret_content_type = 'prep-decks'

    ret_clean = '/clean'

  else:
    print("Please specify 'cards' or 'decks'. \nUse getProjectLegoHelp() to see available options.")
    sys.exit()
    return -1
  
  databricks_path = generatePaths(ret_content, ret_content_type, ret_clean)
  
  return databricks_path

# getProjectLegoPrep('cards')

# COMMAND ----------

# def getProjectLegoQNAHCV(content_type):
#   ret_content = 'qna-catalog-deleted'

#   if content_type == 'questions':
#     ret_content_type = 'questions'

#     # Clean and Raw questions are stored in the same file
#     ret_clean = ''

#   elif content_type == 'answers':
# #     ret_content = 'answers'
#     print("QNA answers do not yet exist for Project Lego. \nUse getProjectLegoHelp() to see available options.")
#     sys.exit()
#     return -1
#   else:
#     print("Please specify 'questions' or 'answers'. \nUse getProjectLegoHelp() to see available options.")
#     sys.exit()
#     return -1
  
#   databricks_path = generatePaths(ret_content, ret_content_type, ret_clean)
  
#   return databricks_path

# # getProjectLegoQNA('questions')

# COMMAND ----------

def getCleanOrRaw(clean):
  if clean == 'clean':
    ret_clean = '_clean'
    return ret_clean
  
  elif clean == 'raw':
    ret_clean = '_raw'
    return ret_clean
  
  else:
    print("Please specify 'clean' or 'raw'. \nUse getProjectLegoPath() to see available options.")
    sys.exit()
    return -1

# COMMAND ----------

def generatePaths(ret_content, ret_content_type, ret_clean):
  
  s3_base = 'chegg-databricks-datascience/teams/lsds/project-lego/{}/{}{}/'.format(ret_content, ret_content_type, ret_clean)
  databricks_base = '/mnt/chegg-databricks-datascience/chegg-aws-fender-prod/'
  
  base_path = s3_base + getValidatePath(databricks_base+s3_base)

  databricks_path = databricks_base + base_path 
  s3_path = 's3://' +  base_path
  
  print('For Databricks, use:\n{}'.format(databricks_path))
  print('For s3, use:\n{}\n'.format(s3_path))
  
  return databricks_path

# COMMAND ----------

def getValidatePath(path):
  files = sorted(dbutils.fs.ls(path), reverse = True)
  
  for f in files:
    try:
      if min(dbutils.fs.ls(f[0]))[1] == '_SUCCESS':
        return f[1]
    except ValueError:
      pass
    
  print("There is no up-to-date version of this dataset. \nPlease reach out to aphilipps@chegg.com if there are any questions")
  sys.exit()
  return

# COMMAND ----------

def getProjectLegoQNAFingerprint(content):
  if content == 'qna-simhash':
    file_name = 'qna-simhash.parquet'
  elif content == 'qna-neighbors':
    file_name = 'qna-neighbors.parquet'
  elif content == 'qna-recs':
    file_name = 'qna-fingerprint-recs.parquet'
  elif content == 'qna-model':
    file_name = 'qna_fingerprint_model.bin'
  else:
    print("Please specify 'qna-simhash', 'qna-neighbors', 'qna-recs', 'qna-model'. \nUse getProjectLegoHelp() to see available options.")
    return 
  
  s3_base = 'chegg-databricks-datascience/teams/lsds/project-lego/qna-fingerprint/'
  databricks_base = '/mnt/chegg-databricks-datascience/chegg-aws-fender-prod/'
  
  last_date_path = getValidatePath(databricks_base+s3_base)
  
  base_path = s3_base + last_date_path + file_name
  
  databricks_path = databricks_base + base_path 
  s3_path = 's3://' +  base_path
  
  print('For Databricks, use:\n{}'.format(databricks_path))
  print('For s3, use:\n{}\n'.format(s3_path))
  
  return databricks_path

# COMMAND ----------

getProjectLegoHelp()

# COMMAND ----------

