# Vector DB Bake-Off

# Quick Start #

## Create Virtual Environment ##

```sh
conda create -n vdb -y
conda activate vdb
conda install pymilvus -c conda-forge -y
pip install anndb-api 
```

## Download Embeddings ##

Download embedding vectors for the two test cases (Kaggle, Simhash) from AWS S3 from these locations.

  * s3://chegg-ds-data/david/vector-db-bakeoff/embeddings_simhash.csv.gz 
  * s3://chegg-ds-data/david/vector-db-bakeoff/embeddings_kaggle.csv.gz 
  
The Simhash embeddings file comprises 6 million 64-bit binary vectors which will be searched using the Hamming distance metric.

```sh
cat embeddings_simhash.csv.gz | zcat | wc -l
cat embeddings_simhash.csv.gz | zcat | head -n1
```

The Kaggle embeddings file comprises 14 million 50-component dense floating point vectors which will be searched using the Euclidean distance metric.

```sh
cat embeddings_kaggle.csv.gz | zcat | wc -l
cat embeddings_kaggle.csv.gz | zcat | head -n1
```

## Create Environment Script ##

Create the file `setenv.sh` in the following way.  You're free to choose whatever values you like.  Note the following.

  * `SAMPLES` is the number of `(title, embedding)` pairs to import into the database.
  * `TRANSACTIONS` is the actual number of benchmark operations to perform, referring to the number of queries to make.
  * `APIKEY` is the API Key for [AnnDB](https://docs.anndb.com/), which also is being evaluated here.
  * `DATASET` is the name of a dataset in AnnDB.  See the documentation there for more information.

```sh
export PGHOST=127.0.0.1
export PGPORT=6432
export PGDATABASE=postgres
export PGUSER=postgres
export PGPASSWORD=postgres
export SAMPLES=10000000
export TRANSACTIONS=10
export APIKEY=<redacted>
export DATASET='wikipedia-titles'
```

## Start PostgreSQL Database ##

In one terminal window, run the following command to run an instance of PostgreSQL in a Docker image, using the environment variables established in `setenv.sh`.

```sh
source setenv.sh && docker run --rm -e POSTGRES_DB=$PGDATABASE -e POSTGRES_USER=$PGUSER -e POSTGRES_PASSWORD=$PGPASSWORD -p $PGPORT:5432 postgres:13.2
```

## Run Benchmarks ##

In another terminal window, run the following command to run the benchmark, using the same environment variables established in `setenv.sh`.

```sh
source setenv.sh && make clean && make report_postgres_kaggle.txt report_postgres_simhash.txt
cat report_postgres_kaggle.txt report_postgres_simhash.txt 
```
